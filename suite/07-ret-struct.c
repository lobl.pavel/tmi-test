/*
@STATUS=27
@OUTPUT="Martin"
*/

#include "../lib/tiny_std.c"

struct person {
    char *name;
    int age;
    int sex;
};

struct person create(char *name, int age, int sex) {
    struct person p;
    p.name = name;
    p.age = age + 1;
    p.sex = sex + 2;
    return p;
}

int main(void) {

    struct person p;
    p = create("Martin", 24, 0);

    prints(p.name);

    return p.age + p.sex;
}
