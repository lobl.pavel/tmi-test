/*
@STATUS=10
*/

struct obj {
    int array[10];
    int len;
};

int sum(const struct obj a) {
    int i, sum = 0;
    for (i = 0; i < a.len; i++)
        sum += a.array[i];
    return sum;
}

int main(void) {
    struct obj a;
    a.len = 5;
    int i;
    for (i = 0; i < a.len; i++)
        a.array[i] = i;
    return sum(a);
}
